#!/usr/bin/python3
import collections
from collections import Counter

freq = 0
answer = []
count = []
seen = set()
duplicates = []

while not answer:
    with open('input.txt') as f:
        for line in f:
            line = line.strip('\n')
            if line[0] == '+':
                freq = freq + int(line[1:])
                count.append(freq)
#                with open("output.txt","a") as file:
#                    file.write("%s\n" % freq)
                answer = [k for k,v in Counter(count).items() if v>1]
                if not answer:
                    pass
                else:
                    print(answer)
                    break

            elif line[0] == '-':
                freq = freq - int(line[1:])
                count.append(freq)
#                with open("output.txt","a") as file:
#                    file.write("%s\n" % freq)
                answer = [k for k,v in Counter(count).items() if v>1]
                if not answer:
                    pass
                else:
                    print(answer)
                    break
