#!/usr/bin/python3

with open('input.txt') as f:
    alist = f.read().splitlines()

blist = alist.copy()

def stringcomp(a, b):
    a = a.strip('\n')
    b = b.strip('\n')
    position = 0
    diff = 0
    for _ in range(26):
        if a[position] == b[position]:
            pass
        else:
            diff = diff + 1
        position = position + 1
    return(diff)

def answer(a, b):
    position = 0
    answer = ""
    for _ in range(26):
        if a[position] == b[position]:
            answer += a[position]
#            position = position + 1
        else:
            pass
        position = position + 1
    return(answer)

for item1 in alist:
    for item2 in blist:
        if stringcomp(item1, item2) == 1:
            print(answer(item1, item2))
        else:
            pass
