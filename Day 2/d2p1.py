#!/usr/bin/python3

import collections

two = 0
three = 0

#Counts the number of instances a letter appears in string and returns a 1
#if any letter/letters are doubled
def twolettercounter(s):
    double = 0
    d = collections.defaultdict(int)
    for c in s:
        d[c] += 1
    for c in sorted(d, key=d.get, reverse=True):
        if d[c] == 2:
            double = 1
        else:
            pass
    return double

#Same as twolettercounter(), but for triple letters
def threelettercounter(s):
    triple = 0
    d = collections.defaultdict(int)
    for c in s:
        d[c] += 1
    for c in sorted(d, key=d.get, reverse=True):
        if d[c] == 3:
            triple = 1
        else:
            pass
    return triple

with open('input.txt') as f:
    for line in f:
        line = line.strip('\n')
        two = twolettercounter(line) + two

with open('input.txt') as f:
    for line in f:
        line = line.strip('\n')
        three = threelettercounter(line) + three

answer = two * three
print("The answer is", answer)
